//module("poker.core.getHandCategory");

test("no ten", function() {
  var nums = [1,1,1,1];
  var result = eval_ten(nums);
  equal(result, 0);
});

test("no ten", function() {
  var nums = [9,9,9,7];
  var result = eval_ten(nums);
  equal(result, 0);
});

test("no ten", function() {
  var nums = [7,3,3,4];
  var result = eval_ten(nums);
  equal(result, 0);
});

test("only add", function() {
  var nums = [1,2,3,4];
  var result = eval_ten(nums);
  equal(result, 1);
});

test("substract used", function() {
  var nums = [3,4,5,2];
  var result = eval_ten(nums);
  equal(result, 1);
});

test("included multiplier", function() {
  var nums = [3,4,0,2];
  var result = eval_ten(nums);
  equal(result, 1);
});

test("division", function() {
  var nums = [9,9,9,5];
  var result = eval_ten(nums);
  equal(result, 1);
});
test("division", function() {
  var nums = [4,8,2,3];
  var result = eval_ten(nums);
  equal(result, 1);
});

test("array(0)", function() {
  var nums = [];
  var result = eval_ten(nums);
  equal(result, 0);
});

test("array(1)", function() {
  var nums = [10];
  var result = eval_ten(nums);
  equal(result, 1);
  
  nums = [1];
  result = eval_ten(nums);
  equal(result, 0);
  
  nums = [new Number(10)];
  result = eval_ten(nums);
  equal(result, 1);
});


//test("array(3) only add", function() {
//  var nums = {};
//  nums[0] = 5;
//  nums[1] = 2;
//  nums[2] = 3;
//  nums.length = 3;
//  var result = eval_ten(nums);
//  equal(result, 1);
//});
//
//test("array(3) multiply after add", function() {
//  var nums = {};
//  nums[0] = 3;
//  nums[1] = 2;
//  nums[2] = 2;
//  nums.length = 3;
//  var result = eval_ten(nums);
//  equal(result, 1);
//});


test("array(14)", function() {
  var nums = [0,0,0,0,0,0,0,0,0,0,0,0,0,0];
  
  var result = eval_ten(nums);
  equal(result, 0);
});

test("3-digit all", function() {
  var cnt = 0;
  var digit = 3;
  for (var i = 0; i < Math.pow(10, digit); i++) {
    var n = i;
    var nums = new Array(digit);
    for (var j = 0; j < digit; j++) {
      nums[digit - j - 1] = n % 10;
      n = Math.floor(n / 10);
    }
    cnt += eval_ten(nums);
  }
  equal(cnt, 226);
});

test("4-digit all", function() {
  var cnt = 0;
  var digit = 4;
  for (var i = 0; i < Math.pow(10, digit); i++) {
    var n = i;
    var nums = new Array(digit);
    for (var j = 0; j < digit; j++) {
      nums[digit - j - 1] = n % 10;
      n = Math.floor(n / 10);
    }
    cnt += eval_ten(nums);
  }
  equal(cnt, 4209);
});

test("5-digit all", function() {
  var cnt = 0;
  var digit = 5;
  for (var i = 0; i < Math.pow(10, digit); i++) {
    var n = i;
    var nums = new Array(digit);
    for (var j = 0; j < digit; j++) {
      nums[digit - j - 1] = n % 10;
      n = Math.floor(n / 10);
    }
    cnt += eval_ten(nums);
  }
  equal(cnt, 68071);
});